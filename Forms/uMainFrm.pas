unit uMainFrm;

{$mode objfpc}{$H+}

{$macro on}
{$define APP_ID:={$I APP_ID.inc}}

interface

uses
  Windows, Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, Menus, ExtCtrls, ComCtrls, Buttons, StdCtrls, ActnList, uAppData,
  CheckLst, uFrmAbout, DOM, XMLRead, XMLWrite, LCLIntf, httpsend, dateutils;

type

  TSortType = (stDate, stUser, stAlphabet, stLength);

  TListItem = class
    Value:    string;
    Finished: Boolean;
    Date:     TDateTime;
  end;

  { TCheckList }

  TCheckList = class
    constructor Create();
    destructor Destroy(); override;
  public
    Name:  string;
    Items: array of TListItem;
  end;

  { TMainFrm }

  TMainFrm = class(TForm)
    acCheckList: TAction;
    acReverseCheck: TAction;
    acItemUp: TAction;
    acItemDown: TAction;
    acSortItems: TAction;
    acEditItem: TAction;
    acRemoveItem: TAction;
    acEditListName: TAction;
    acRemoveList: TAction;
    acShowChangeLog: TAction;
    acDonate: TAction;
    acAbout: TAction;
    acUpdate: TAction;
    acWebsite: TAction;
    acUncheckList: TAction;
    CheckListBox: TCheckListBox;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    ImageListSmall: TImageList;
    ListBox: TListBox;
    MenuItem10: TMenuItem;
    MenuItem11: TMenuItem;
    MenuItem12: TMenuItem;
    MenuItem13: TMenuItem;
    MenuItem14: TMenuItem;
    MenuItem15: TMenuItem;
    MenuItem16: TMenuItem;
    MenuItem17: TMenuItem;
    MenuItem18: TMenuItem;
    MenuItem19: TMenuItem;
    MenuItem20: TMenuItem;
    MenuItem21: TMenuItem;
    MenuItem22: TMenuItem;
    MenuItem23: TMenuItem;
    MenuItem24: TMenuItem;
    MenuItem25: TMenuItem;
    MenuItem26: TMenuItem;
    MenuItem27: TMenuItem;
    MenuItem28: TMenuItem;
    MenuItem29: TMenuItem;
    MenuItem30: TMenuItem;
    miChangeLog: TMenuItem;
    MenuItem32: TMenuItem;
    MenuItem33: TMenuItem;
    MenuItem34: TMenuItem;
    MenuItem35: TMenuItem;
    MenuItem36: TMenuItem;
    MenuItem37: TMenuItem;
    MenuItem8: TMenuItem;
    MenuItem9: TMenuItem;
    acNewItem: TAction;
    acNewList: TAction;
    ActionList: TActionList;
    ImageList: TImageList;
    MainMenu: TMainMenu;
    MenuItem1: TMenuItem;
    MenuItem2: TMenuItem;
    MenuItem3: TMenuItem;
    MenuItem4: TMenuItem;
    MenuItem5: TMenuItem;
    MenuItem6: TMenuItem;
    MenuItem7: TMenuItem;
    PageControl: TPageControl;
    PMSort: TPopupMenu;
    PMItems: TPopupMenu;
    PMLists: TPopupMenu;
    Splitter: TSplitter;
    TabSheet1: TTabSheet;
    ToolBar: TToolBar;
    ToolButton1: TToolButton;
    ToolButton10: TToolButton;
    ToolButton11: TToolButton;
    ToolButton2: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    ToolButton8: TToolButton;
    ToolButton9: TToolButton;
    procedure acAboutExecute(Sender: TObject);
    procedure acCheckListExecute(Sender: TObject);
    procedure acDonateExecute(Sender: TObject);
    procedure acEditItemExecute(Sender: TObject);
    procedure acEditListNameExecute(Sender: TObject);
    procedure acItemDownExecute(Sender: TObject);
    procedure acItemUpExecute(Sender: TObject);
    procedure acRemoveItemExecute(Sender: TObject);
    procedure acRemoveListExecute(Sender: TObject);
    procedure acReverseCheckExecute(Sender: TObject);
    procedure acShowChangeLogExecute(Sender: TObject);
    procedure acSortItemsExecute(Sender: TObject);
    procedure acUncheckListExecute(Sender: TObject);
    procedure acUpdateExecute(Sender: TObject);
    procedure acWebsiteExecute(Sender: TObject);
    procedure CheckListBoxItemClick(Sender: TObject; Index: integer);
    procedure CheckListBoxMouseDown(Sender: TObject; {%H-}Button: TMouseButton; {%H-}Shift: TShiftState; X, Y: Integer);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure ListBoxMouseDown(Sender: TObject; {%H-}Button: TMouseButton; {%H-}Shift: TShiftState; X, Y: Integer);
    procedure ListBoxSelectionChange(Sender: TObject; User: boolean);
    procedure MenuItem18Click(Sender: TObject);
    procedure acNewItemExecute(Sender: TObject);
    procedure acNewListExecute(Sender: TObject);
  private
    //Details: TStringList;

    SortType: TSortType;
    Ascending: Boolean;

    function GetSelectedList: TCheckList;

    procedure SetSortType(const AValue: TSortType; const AAscending: Boolean);

    procedure LoadFromFile();
    procedure SaveToFile();

    procedure DisplayList(const Index: Integer);

    procedure SwapCheckBox(const IndexA, IndexB: Integer);

    procedure SortCheckListBox();

    { private declarations }
  public
    property SelectedList: TCheckList read GetSelectedList;

    { public declarations }
  end;

var
  MainFrm: TMainFrm;

function TextResource(const ResName: string): string;

implementation

const
  XMLFileName   = 'checklist.xml';
  ChangeLogFile = 'changelog.txt';

function TextResource(const ResName: string): string;
var
  Stream: TResourceStream;
begin

  try

    Stream := TResourceStream.Create(HINSTANCE, ResName, RT_RCDATA);
    try
      Stream.Position := 0;

      SetLength(Result, Stream.Size);
      if (Stream.Size > 0) then
        Stream.ReadBuffer(Result[1], Stream.Size);
    finally
      Stream.Free();
    end;

  except
    Result := '';
  end;

end;

{$R *.lfm}

{ TCheckList }

constructor TCheckList.Create;
begin
  inherited Create();
  SetLength(Items, 0);
end;

destructor TCheckList.Destroy;
var
  i: Integer;
begin
  for i := Low(Items) to High(Items) do
    Items[i].Free();
  SetLength(Items, 0);
  inherited Destroy;
end;

{ TMainFrm }

procedure TMainFrm.FormCreate(Sender: TObject);
var
  i: Integer;
begin
  LoadVersionInfo();

  miChangeLog.Enabled := FileExists(ChangeLogFile);

  SortType     := stUser;
  Ascending    := True;

  PageControl.ActivePageIndex := 0;

  for i := 0 to ActionList.ActionCount - 1 do
    with TAction(ActionList.Actions[i]) do
      Hint := Caption;

  LoadFromFile();
end;

procedure TMainFrm.CheckListBoxItemClick(Sender: TObject; Index: integer);
begin
  TListItem(CheckListBox.Items.Objects[Index]).Finished := CheckListBox.Checked[Index];
end;

procedure TMainFrm.CheckListBoxMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  CheckListBox.ItemIndex := CheckListBox.ItemAtPos(Point(X, Y), True);
end;

procedure TMainFrm.acCheckListExecute(Sender: TObject);
var
  i: Integer;
begin
  for i := 0 to CheckListBox.Count - 1 do
  begin
    CheckListBox.Checked[i] := True;
    TListItem(CheckListBox.Items.Objects[i]).Finished := True;
  end;
end;

procedure TMainFrm.acAboutExecute(Sender: TObject);
begin
  TFrmAbout.Execute();
end;

procedure TMainFrm.acDonateExecute(Sender: TObject);
begin
 OpenURL(Format('http://www.sciresoft.nl/donate-%.4d.html', [APP_ID]));
end;

procedure TMainFrm.acEditItemExecute(Sender: TObject);
var
  S: string;
begin
  with CheckListBox do
    if (ItemIndex > -1) then
    begin
      S := Items.Strings[ItemIndex];
      if (InputQuery('Edit Item', 'Edit this item:', S)) then
      begin
        Items.Strings[ItemIndex] := S;
        TListItem(Items.Objects[ItemIndex]).Value := S;
      end;
    end;
end;

procedure TMainFrm.acEditListNameExecute(Sender: TObject);
var
  ListName: string;
begin
  if (Assigned(SelectedList)) then
  begin
    ListName := SelectedList.Name;

    if (InputQuery('Edit List', 'Edit this list name:', ListName)) then
    begin
      ListBox.Items.Strings[ListBox.ItemIndex] := ListName;
      SelectedList.Name := ListName;
    end;

  end;
end;

procedure TMainFrm.acItemDownExecute(Sender: TObject);
begin
  with CheckListBox do
    if (Count > 1) and (ItemIndex > -1) and (ItemIndex < Items.Count - 1) then
    begin
      SwapCheckBox(ItemIndex, ItemIndex + 1);
      ItemIndex := ItemIndex + 1;
    end;
end;

procedure TMainFrm.acItemUpExecute(Sender: TObject);
begin
  with CheckListBox do
    if (Count > 1) and (ItemIndex > 0) then
    begin
      SwapCheckBox(ItemIndex, ItemIndex - 1);
      ItemIndex := ItemIndex - 1;
    end;
end;

procedure TMainFrm.acRemoveItemExecute(Sender: TObject);
var
  i: Integer;
begin
  if (not Assigned(SelectedList)) then Exit;

  with CheckListBox do
    if (ItemIndex > -1) then
    begin
      if (MessageDlg('Delete Item', 'Are you sure?', mtConfirmation, mbYesNo, 0) = mrYes) then
      begin

        SelectedList.Items[ItemIndex].Free();

        for i := ItemIndex to High(SelectedList.Items) - 1 do
          SelectedList.Items[i] := SelectedList.Items[i + 1];

        SetLength(SelectedList.Items, Length(SelectedList.Items) - 1);

        Items.Delete(ItemIndex);
      end;
    end;
end;

procedure TMainFrm.acRemoveListExecute(Sender: TObject);
var
  {%H-}i: Integer;
begin
  if (not Assigned(SelectedList)) then Exit;

  if (MessageDlg('Delete List', 'Are you sure?', mtConfirmation, mbYesNo, 0) = mrYes) then
  begin
    ListBox.Items.Objects[ListBox.ItemIndex].Free();
    ListBox.Items.Objects[ListBox.ItemIndex] := nil;

    ListBox.Items.Delete(ListBox.ItemIndex);
    if (ListBox.Count > 0) then
      DisplayList(0)
    else CheckListBox.Clear();

  end;

end;

procedure TMainFrm.acReverseCheckExecute(Sender: TObject);
var
  i: Integer;
begin
  for i := 0 to CheckListBox.Count - 1 do
  begin
    CheckListBox.Checked[i] := not CheckListBox.Checked[i];
    TListItem(CheckListBox.Items.Objects[i]).Finished := CheckListBox.Checked[i];
  end;
end;

procedure TMainFrm.acShowChangeLogExecute(Sender: TObject);
begin
  if (FileExists(ChangeLogFile)) then
    OpenDocument(ChangeLogFile);
end;

procedure TMainFrm.acSortItemsExecute(Sender: TObject);
begin
  SetSortType(SortType, not Ascending);
end;

procedure TMainFrm.acUncheckListExecute(Sender: TObject);
var
  i: Integer;
begin
  for i := 0 to CheckListBox.Count - 1 do
  begin
    CheckListBox.Checked[i] := False;
    TListItem(CheckListBox.Items.Objects[i]).Finished := False;
  end;
end;

procedure TMainFrm.acUpdateExecute(Sender: TObject);

type
  TResponse = packed record
    Major, Minor, Release, Build: Word;
    BuildDate: Integer;
  end;

var
  Res: TResponse;
begin
  with THTTPSend.Create() do
  try

    if (HTTPMethod('GET', 'http://api.sciresoft.nl/info.php?ID=' + IntToStr(APP_ID))) then
    begin
      Document.Position := 0;
      if (Document.Size >= SizeOf(Res)) then
        Document.ReadBuffer(Res{%H-}, SizeOf(Res))
      else Exit;
    end;

  finally
    Free();
  end;

  Res.Build := 0; // Block update part

  if (Res.Build > APP_VERSION_BUILD) then
  begin
    if (MessageDlg('Update', 'A newer version is available. Do you want to go to the downloadspage?'#13#10 +
                    Format('Version: %d.%d.%d [0x%.4X]', [Res.Major, Res.Minor, Res.Release, Res.Build]) + #13#10 +
                    'Build on: ' + DateTimeToStr(UnixToDateTime(Res.BuildDate)), mtConfirmation, mbYesNo, 0) = mrYes) then
                      acWebsite.Execute();
  end else
    MessageDlg('Update', 'No newer version is available.', mtInformation, [mbOK], 0);

end;

procedure TMainFrm.acWebsiteExecute(Sender: TObject);
begin
  OpenURL(Format('http://www.sciresoft.nl/productview-%.4d.html', [APP_ID]));
end;

procedure TMainFrm.FormDestroy(Sender: TObject);
var
  i: Integer;
begin
  SaveToFile();

  for i := 0 to ListBox.Count - 1 do
  begin
    ListBox.Items.Objects[i].Free();
    ListBox.Items.Objects[i] := nil;
  end;

  //Details.Free();

end;

procedure TMainFrm.ListBoxMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  Index: Integer;
begin
  Index := ListBox.ItemAtPos(Point(X, Y), True);
  if (Index > -1) then DisplayList(Index);
end;

procedure TMainFrm.ListBoxSelectionChange(Sender: TObject; User: boolean);
begin
  if (User) then
    DisplayList(ListBox.ItemIndex);
end;

procedure TMainFrm.MenuItem18Click(Sender: TObject);
var
  ItmTag: Integer;
const
  TAG_ASC  = 1;

  TAG_ALPA = 2;
  TAG_DATE = 4;
  TAG_USER = 8;
  TAG_LEN  = 16;

  function IsTag(const Val: Integer): Boolean;
  begin
    Result := ((ItmTag and Val) = Val);
  end;

begin
  ItmTag := TMenuItem(Sender).Tag;

  if (IsTag(TAG_ALPA)) then
    SetSortType(stAlphabet, IsTag(TAG_ASC))
  else if (IsTag(TAG_DATE)) then
    SetSortType(stDate, IsTag(TAG_ASC))
  else if (IsTag(TAG_USER)) then
    SetSortType(stUser, False)
  else if (IsTag(TAG_LEN)) then
    SetSortType(stLength, IsTag(TAG_ASC))

end;

procedure TMainFrm.acNewItemExecute(Sender: TObject);
var
  ItemName: string;
  len: Integer;
begin
  if (not Assigned(SelectedList)) then Exit;

  ItemName := 'New Item #' + IntToStr(CheckListBox.Count + 1);
  if (InputQuery('New Item', 'Enter a item:', ItemName)) then
  begin

    len := Length(SelectedList.Items);
    SetLength(SelectedList.Items, len + 1);

    SelectedList.Items[len] := TListItem.Create();

    with SelectedList.Items[len] do
    begin
      Value     := ItemName;
      Date      := Now;
      Finished  := False;
    end;

    CheckListBox.Items.AddObject(ItemName, SelectedList.Items[len]);

    SortCheckListBox();
  end;

end;

procedure TMainFrm.acNewListExecute(Sender: TObject);
var
  ListName: string;
  List: TCheckList;
begin

  ListName := 'New List #' + IntToStr(ListBox.Count + 1);
  if (InputQuery('New List', 'Enter a list name:', ListName)) then
  begin

    List := TCheckList.Create();
    List.Name := ListName;

    DisplayList(ListBox.Items.AddObject(ListName, List));
  end;
end;

function TMainFrm.GetSelectedList: TCheckList;
begin
  if (ListBox.ItemIndex > -1) then
    Result := TCheckList(ListBox.Items.Objects[ListBox.ItemIndex])
  else
    Result := nil;
end;

procedure TMainFrm.SetSortType(const AValue: TSortType; const AAscending: Boolean);
begin
  if (SortType = AValue) and (Ascending = AAscending) then Exit;
  SortType := AValue;

  case SortType of
    stDate:     acSortItems.ImageIndex := 5;
    stAlphabet: acSortItems.ImageIndex := 7;
    stLength:   acSortItems.ImageIndex := 9;
    stUser:     acSortItems.ImageIndex := 11;
  end;

  Ascending := AAscending;

  if (not Ascending) and (SortType <> stUser) then
    acSortItems.ImageIndex := acSortItems.ImageIndex + 1;

  SortCheckListBox();
end;

procedure TMainFrm.LoadFromFile;
var
  XML: TXMLDocument;
  i, j: Integer;
  List: TCheckList;
begin
  if (not FileExists(XMLFileName)) then Exit;

  ReadXMLFile(XML, XMLFileName);
  try

    for i := 0 to XML.DocumentElement.ChildNodes.Count - 1 do
      with XML.DocumentElement.ChildNodes[i] do
      begin
        List := TCheckList.Create();
        List.Name := Attributes.GetNamedItem('name').NodeValue;

        SetLength(List.Items, ChildNodes.Count);
        for j := 0 to ChildNodes.Count - 1 do
          with ChildNodes[j] do
          begin

            List.Items[j] := TListItem.Create();

            List.Items[j].Date      := StrToFloatDef(Attributes.GetNamedItem('date').NodeValue, 0);
            List.Items[j].Finished  := StrToBoolDef(Attributes.GetNamedItem('finish').NodeValue, False);
            List.Items[j].Value     := StringReplace(Attributes.GetNamedItem('text').NodeValue, '&quot;', '"', [rfReplaceAll, rfIgnoreCase]);

          end;

        ListBox.Items.AddObject(List.Name, List);
      end;


  finally
    XML.Free();
  end;


  if (ListBox.Count > 0) then DisplayList(0);
end;

procedure TMainFrm.SaveToFile;
var
  XML: TXMLDocument;
  i, j: Integer;
  RootNode, ListNode, ItemNode: TDOMNode;
  List: TCheckList;
begin

  XML := TXMLDocument.Create();
  try

    RootNode := XML.CreateElement('root');
    XML.AppendChild(RootNode);

    for i := 0 to ListBox.Count - 1  do
    begin
      ListNode := XML.CreateElement('list');
      List := TCheckList(ListBox.Items.Objects[i]);

      TDOMElement(ListNode).SetAttribute('name', List.Name);
      for j := Low(List.Items) to High(List.Items) do
      begin
        ItemNode := XML.CreateElement('item');

        TDOMElement(ItemNode).SetAttribute('date',    FloatToStr(List.Items[j].Date));
        TDOMElement(ItemNode).SetAttribute('finish',  BoolToStr(List.Items[j].Finished));
        TDOMElement(ItemNode).SetAttribute('text',    StringReplace(List.Items[j].Value, '"', '&quot;', [rfIgnoreCase, rfReplaceAll]));

        ListNode.AppendChild(ItemNode);
      end;

      XML.DocumentElement.AppendChild(ListNode);
    end;

    WriteXMLFile(XML, XMLFileName);

  finally
    XML.Free();
  end;

end;

procedure TMainFrm.DisplayList(const Index: Integer);
var
  i: Integer;
begin
  CheckListBox.Clear();

  ListBox.ItemIndex := Index;

  if (Assigned(SelectedList)) then
    with SelectedList do
      for i := Low(Items) to High(Items) do
          CheckListBox.Checked[CheckListBox.Items.AddObject(Items[i].Value, Items[i])] := Items[i].Finished;

  SetSortType(stUser, False);
end;

procedure TMainFrm.SwapCheckBox(const IndexA, IndexB: Integer);
var
  sTMP: string;
  bTMP: Boolean;
  pTMP: TDateTime;
begin

  if (not Assigned(SelectedList)) or (IndexA < 0) or (IndexB < 0) then Exit;

  with SelectedList do
  begin

    with Items[IndexA] do
    begin
      sTMP := Value;
      bTMP := Finished;
      pTMP := Date;

      Value    := Items[IndexB].Value;
      Finished := Items[IndexB].Finished;
      Date     := Items[IndexB].Date;
    end;

    with Items[IndexB] do
    begin
      Value    := sTMP;
      Finished := bTMP;
      Date     := pTMP;
    end;

  end;


  with CheckListBox do
  begin
    Items.Strings[IndexA] := TListItem(Items.Objects[IndexA]).Value;
    Checked[IndexA]       := TListItem(Items.Objects[IndexA]).Finished;

    Items.Strings[IndexB] := TListItem(Items.Objects[IndexB]).Value;
    Checked[IndexB]       := TListItem(Items.Objects[IndexB]).Finished;
  end;
end;

procedure TMainFrm.SortCheckListBox;

  function Compare(const IndexA, IndexB: Integer): Boolean;
  begin
    with CheckListBox do
      case SortType of
        stAlphabet: Result := (CompareText(Items[IndexA], Items[IndexB]) > 0);
        stDate:     Result := (TListItem(Items.Objects[IndexA]).Date > TListItem(Items.Objects[IndexB]).Date);
        stLength:   Result := (Length(Items[IndexA]) > Length(Items[IndexB]));
      else          Result := False;
      end;
  end;

var
  i, j: Integer;
  Comp: Boolean;
begin
  if (SortType = stUser) then Exit;

  for i := CheckListBox.Count - 1 downto 0 do
    for j := 0 to i - 1 do
    begin
      Comp := Compare(j, j + 1);
      if (not Ascending) then Comp := not Comp;

      if (Comp) then
        SwapCheckBox(j, j + 1);
    end;

end;

end.

