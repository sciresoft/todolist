unit uFrmAbout;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ExtCtrls, ComCtrls, StdCtrls, uAppData, HtmlView,
  LCLIntf, HtmlGlobals;

type

  { TFrmAbout }

  TFrmAbout = class(TForm)
    Button1: TButton;
    Button2: TButton;
    Image: TImage;
    lblAppName: TLabel;
    lblVersion: TLabel;
    HTML: THtmlViewer;
    Panel1: TPanel;
    Panel2: TPanel;
    TabControl: TTabControl;
    procedure FormCreate(Sender: TObject);
    procedure HotSpotClick(Sender: TObject; const SRC: ThtString; var Handled: Boolean);
    procedure TabControlChange(Sender: TObject);
  private
    procedure ActivateTab(const AIndex: Integer);
    { private declarations }
  public
    class function Execute(): TModalResult;
    { public declarations }
  end;

implementation

uses uMainFrm;

{$R *.lfm}

{ TFrmAbout }

procedure TFrmAbout.FormCreate(Sender: TObject);
begin
  ActivateTab(0);

  lblVersion.Caption := Format('%.d.%d.%d [0x%.4X]', [APP_VERSION_MAJOR, APP_VERSION_MINOR, APP_VERSION_RELEASE, APP_VERSION_BUILD]);
  lblAppName.Caption := APP_NAME;

  Image.Picture.Assign(Application.Icon);
end;

procedure TFrmAbout.HotSpotClick(Sender: TObject; const SRC: ThtString; var Handled: Boolean);
begin
  OpenURL(SRC);
  Handled := True;
end;

procedure TFrmAbout.TabControlChange(Sender: TObject);
begin
  ActivateTab(TabControl.TabIndex);
end;

procedure TFrmAbout.ActivateTab(const AIndex: Integer);
begin
  case AIndex of
    0: HTML.LoadFromString(TextResource('CREDITS'));
    1: HTML.LoadFromString(TextResource('LICENSE'));
    2: HTML.LoadFromString(TextResource('DONATE'));
    3: HTML.LoadFromString(StringReplace(TextResource('README'), #13, '<br />', [rfIgnoreCase, rfReplaceAll]));
  end;

  TabControl.TabIndex := AIndex;
end;

class function TFrmAbout.Execute: TModalResult;
begin
  with TFrmAbout.Create(Application) do
  try
    Result := ShowModal();
  finally
    Free();
  end;
end;

end.

