unit uAppData;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, uFileInfo;

const
   APP_DEBUG: 		    Boolean = False;
   APP_PATCHED: 	    Boolean = False;
   APP_PRERELEASE: 	  Boolean = False;
   APP_PRIVATEBUILD: 	Boolean = False;
   APP_SPECIALBUILD: 	Boolean = False;

   APP_BUILD_DATE: TDateTime = 0;
   APP_VERSION_MAJOR: 	Word = 0;
   APP_VERSION_MINOR: 	Word = 0;
   APP_VERSION_RELEASE: Word = 0;
   APP_VERSION_BUILD: 	Word = 0;
   APP_NAME: 		string = '';
   APP_COMPANY: string = '';

   EXE_FOLDER	:  string = '';
   EXE_NAME		:  string = '';

procedure LoadVersionInfo();
function CreateDirTree(const Dir: string): Boolean;

implementation

const
   MAXBYTE = 255;

var
	VER_LOADED : Boolean = False;

function VendorName(): string;
begin
  Result := APP_COMPANY;
end;

function ApplicationName(): string;
begin
  Result := APP_NAME;
end;

procedure LoadVersionInfo();

const
  VS_FF_DEBUG 				= $00000001;
  //VS_FF_INFOINFERRED = $00000010;
  VS_FF_PATCHED 			= $00000004;
  VS_FF_PRERELEASE 	  = $00000002;
  VS_FF_PRIVATEBUILD  = $00000008;
  VS_FF_SPECIALBUILD  = $00000020;

var
	dwFileFlags: DWORD = 0;
  APP_PATH: string;
begin
  if (VER_LOADED) then Exit;

  SetLength(APP_NAME, MAXBYTE);
  SetLength(APP_COMPANY, MAXBYTE);

  SetLength(APP_PATH, MAX_PATH);

  FillChar(APP_NAME[1], Length(APP_NAME), 0);
  FillChar(APP_COMPANY[1], Length(APP_COMPANY), 0);
  FillChar(APP_PATH[1], Length(APP_PATH), 0);

  GetVersionInfo(HINSTANCE,
  @APP_VERSION_MAJOR, @APP_VERSION_MINOR, @APP_VERSION_RELEASE, @APP_VERSION_BUILD,
  nil, nil, nil, nil,
  @dwFileFlags, nil, nil, nil,
  @APP_BUILD_DATE,
  nil,
  @APP_COMPANY[1], nil, nil, nil, nil, nil, @APP_NAME[1], nil,
  @APP_PATH[1]);

  APP_NAME 			:= Trim(APP_NAME);
  APP_COMPANY   := Trim(APP_COMPANY);
  APP_PATH 			:= Trim(APP_PATH);

  Boolean((@APP_DEBUG)^) 	:= ((dwFileFlags and VS_FF_DEBUG) 	 					 = VS_FF_DEBUG);
  Boolean((@APP_PATCHED)^) 	:= ((dwFileFlags and VS_FF_PATCHED) 	 			 = VS_FF_PATCHED);
  Boolean((@APP_PRERELEASE)^) 	:= ((dwFileFlags and VS_FF_PRERELEASE) 	 = VS_FF_PRERELEASE);
  Boolean((@APP_PRIVATEBUILD)^) := ((dwFileFlags and VS_FF_PRIVATEBUILD) = VS_FF_PRIVATEBUILD);
  Boolean((@APP_SPECIALBUILD)^) := ((dwFileFlags and VS_FF_SPECIALBUILD) = VS_FF_SPECIALBUILD);

  EXE_FOLDER 	:= IncludeTrailingPathDelimiter(ExtractFilePath(APP_PATH));
  EXE_NAME    := ExtractFileName(APP_PATH);

  OnGetVendorName       := @VendorName;
  OnGetApplicationName  := @ApplicationName;

  VER_LOADED := True;
end;

function CreateDirTree(const Dir: string): Boolean;
var
  parent: string;
begin
  parent := ExtractFilePath(ExtractFileDir(Dir));
  if ((not DirectoryExists(parent)) and (not CreateDirTree(parent))) then Exit(False);
  Result := CreateDir(Dir);
end;

end.

