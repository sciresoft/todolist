unit uFileInfo;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, resource, versionresource;

const MAXBYTE: Byte = $FF;

procedure GetVersionInfo(const AInstance: TFPResourceHMODULE;
  const wFileVersionMajor, wFileVersionMinor, wFileVersionRelease, wFileVersionBuild, wProductVersionMajor, wProductVersionMinor, wProductVersionRelease, wProductVersionBuild: PWORD;
  const dwFileFlags, dwFileOS, dwFileType, dwFileSubtype: PDWORD;
  const dtFileDate: PDateTime;
  const {%H-}wLanguageID: PWORD;
  const sCompanyName, sOriginalFilename, sInternalName, sFileDescription, sLegalCopyright, sLegalTrademarks, sProductName, sComments: PChar;
  const sModulePath: PChar);

implementation

const
  {%H-}BaseLangID = $04E4;
  StringNames: array [ 0 .. 9 ] of string = (
    'CompanyName',
    'OriginalFilename',
    'InternalName',
    'FileDescription',
    'LegalCopyright',
    'LegalTrademarks',
    'ProductName',
    'Comments',
    'FileVersion',
    'ProductVersion'
    );

  STR_COMPANY_NAME 	= 0;
  STR_ORIGINAL_FILENAME = 1;
  STR_INTERNAL_NAME 	= 2;
  STR_FILE_DESCRIPTION 	= 3;
  STR_LEGAL_COPYRIGTH 	= 4;
  STR_LEGAL_TRADEMARKS 	= 5;
  STR_PRODUCT_NAME 	= 6;
  STR_COMMENTS 		= 7;
  {%H-}STR_FILE_VERSION 	= 8;
  {%H-}STR_PROCDUCT_VERSION 	= 9;


procedure CopyAssigned(const Source; const Des: Pointer; const dwSize: DWORD);
begin
  if (Assigned(Des)) then Move(Source, Des^, dwSize);
end;

procedure GetVersionInfo(const AInstance: TFPResourceHMODULE;
  const wFileVersionMajor, wFileVersionMinor, wFileVersionRelease, wFileVersionBuild, wProductVersionMajor, wProductVersionMinor, wProductVersionRelease, wProductVersionBuild: PWORD;
  const dwFileFlags, dwFileOS, dwFileType, dwFileSubtype: PDWORD;
  const dtFileDate: PDateTime;
  const wLanguageID: PWORD;
  const sCompanyName, sOriginalFilename, sInternalName, sFileDescription, sLegalCopyright, sLegalTrademarks, sProductName, sComments: PChar;
  const sModulePath: PChar);
var
  VersResource: TVersionResource;
  Stream: TResourceStream;

  AFileDate: TDateTime;
  ModName: string;
begin
  VersResource := TVersionResource.Create();
  try

    try

      Stream := TResourceStream.CreateFromID(AInstance, 1, PChar(RT_VERSION));
			try
	    	VersResource.SetCustomRawDataStream(Stream);
	     	VersResource.FixedInfo;
	     	VersResource.SetCustomRawDataStream(nil);
	  	finally
	    	Stream.Free();
	  	end;

    except
    	Exit;
    end;

  	CopyAssigned(VersResource.FixedInfo.FileVersion[0], wFileVersionMajor, SizeOf(Word));
  	CopyAssigned(VersResource.FixedInfo.FileVersion[1], wFileVersionMinor, SizeOf(Word));
  	CopyAssigned(VersResource.FixedInfo.FileVersion[2], wFileVersionRelease, SizeOf(Word));
  	CopyAssigned(VersResource.FixedInfo.FileVersion[3], wFileVersionBuild, SizeOf(Word));

  	CopyAssigned(VersResource.FixedInfo.ProductVersion[0], wProductVersionMajor, SizeOf(Word));
  	CopyAssigned(VersResource.FixedInfo.ProductVersion[1], wProductVersionMinor, SizeOf(Word));
  	CopyAssigned(VersResource.FixedInfo.ProductVersion[2], wProductVersionRelease, SizeOf(Word));
  	CopyAssigned(VersResource.FixedInfo.ProductVersion[3], wProductVersionBuild, SizeOf(Word));

    CopyAssigned(VersResource.FixedInfo.FileFlags, dwFileFlags, SizeOf(DWORD));
    CopyAssigned(VersResource.FixedInfo.FileOS, dwFileOS, SizeOf(DWORD));
    CopyAssigned(VersResource.FixedInfo.FileType, dwFileType, SizeOf(DWORD));
    CopyAssigned(VersResource.FixedInfo.FileSubType, dwFileSubtype, SizeOf(DWORD));

    //DateTimeToFileDate

    AFileDate := 0;

    //AFileDate := TimeStampToDateTime(TTimeStamp(VersResource.FixedInfo.FileDate));
    CopyAssigned(AFileDate, dtFileDate, SizeOf(AFileDate));

    if (VersResource.StringFileInfo.Count > 0) then
    with VersResource.StringFileInfo.Items[0] do
    begin

      CopyAssigned(Values[StringNames[STR_COMPANY_NAME]][1], 				sCompanyName, Length(Values[StringNames[STR_COMPANY_NAME]]));
      CopyAssigned(Values[StringNames[STR_ORIGINAL_FILENAME]][1], 	sOriginalFilename, Length(Values[StringNames[STR_ORIGINAL_FILENAME]]));
      CopyAssigned(Values[StringNames[STR_INTERNAL_NAME]][1], 			sInternalName, Length(Values[StringNames[STR_INTERNAL_NAME]]));
      CopyAssigned(Values[StringNames[STR_FILE_DESCRIPTION]][1], 		sFileDescription, Length(Values[StringNames[STR_FILE_DESCRIPTION]]));
      CopyAssigned(Values[StringNames[STR_LEGAL_COPYRIGTH]][1], 		sLegalCopyright, Length(Values[StringNames[STR_LEGAL_COPYRIGTH]]));
      CopyAssigned(Values[StringNames[STR_LEGAL_TRADEMARKS]][1], 		sLegalTrademarks, Length(Values[StringNames[STR_LEGAL_TRADEMARKS]]));
      CopyAssigned(Values[StringNames[STR_PRODUCT_NAME]][1], 				sProductName, Length(Values[StringNames[STR_PRODUCT_NAME]]));
      CopyAssigned(Values[StringNames[STR_COMMENTS]][1], 						sComments, Length(Values[StringNames[STR_COMMENTS]]));

    end;

    ModName := GetModuleName(AInstance);
    if (Length(ModName) = 0) then ModName := ParamStr(0);

    CopyAssigned(ModName[1], sModulePath, Length(ModName));

//    for i := 0 to VersResource.StringFileInfo.Count - 1 do
//    	for j := 0 to VersResource.StringFileInfo.Items[i].Count - 1 do
//        MessageDlg(VersResource.StringFileInfo.Items[i].Keys[j], VersResource.StringFileInfo.Items[i].ValuesByIndex[j], mtConfirmation, mbOKCancel, 0);



//    Move(VersResource.StringFileInfo.Items[0].Keys[0], wFileVersionMajor^, 255);

  finally
    VersResource.Free();
  end;

end;

end.
